Code for microcontroler in Turris 1.1 which takes care of power control.

How to build it:

1) download ARM GCC compiler from https://launchpad.net/gcc-arm-embedded and
	extract it to folder ~HOME/armembedded/toolchain 
	- this path is used in makefile
	(version GCC ARM Embedded 4.8-2014-q3-update was used.)

2) If a 64-bit Linux OS is being used, install 32-bit libraries. In Ubuntu based Distros, type:
 	"sudo apt-get install ia32-libs"

3) add the toolchain’s “bin” directory to the “PATH” environment variable. 
	"nano ~/.bashrc"
	and add the following line at the end of the .bashrc file:
	"export PATH=$PATH:$HOME/armembedded/toolchain/gcc-arm-none-eabi-4_8-2014q3/bin"
	
	exit nano
	refresh the changes in current terminal: "source ~/.bashrc"

4) for build just type "make" in main directory (where the makefile is stored)


How to flash it:

1) download and unpack OpenOCD http://sourceforge.net/projects/openocd/files/openocd/
	(version Open On-Chip Debugger 0.8.0 was used)

2) if required, install dependencies: "sudo apt-get install git zlib1g-dev libtool flex bison libgmp3-dev libmpfr-dev libncurses5-dev libmpc-dev autoconf texinfo build-essential libftdi-dev libusb-1.0.0-dev"

3) type in OpenOCD unpacked directory:
		"./configure"  
		"make" 
		"sudo make install"

4) flashing: in main directory type:

when stm32f0 discovery kit is used as a flasher: 
 "sudo openocd -s /usr/local/share/openocd/scripts -f interface/stlink-v2.cfg -f target/stm32f0x_stlink.cfg -c "init" -c "sleep 200" -c "reset init" -c "sleep 200" -c "reset halt" -c "sleep 100" -c "wait_halt 2" -c "flash write_image erase turris_power_control.bin 0x08000000" -c "sleep 100" -c "verify_image turris_power_control.bin 0x08000000" -c "sleep 100" -c "reset run" -c shutdown"

when stm32f0 nucleo kit is used as a flasher:
 "sudo openocd -s /usr/local/share/openocd/scripts -f interface/stlink-v2-1.cfg -f target/stm32f0x_stlink.cfg -c "init" -c "sleep 200" -c "reset init" -c "sleep 200" -c "reset halt" -c "sleep 100" -c "wait_halt 2" -c "flash write_image erase turris_power_control.bin 0x08000000" -c "sleep 100" -c "verify_image turris_power_control.bin 0x08000000" -c "sleep 100" -c "reset run" -c shutdown"
