/**
  ******************************************************************************
  * @file    hw_config.h
  * @author  J. Kovanda & T. Rykl & Z. Kos
  * @date    18-April-2014
  * @brief   Header file HW config file
  ******************************************************************************
  ******************************************************************************
**/

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __HW_CONFIG_H
#define __HW_CONFIG_H

#include "stm32f0xx.h"

/* Function prototypes -------------------------------------------------------*/

/*******************************************************************************
  * @function   Systick_Config
  * @brief      Setup SysTick Timer for 1 msec interrupts.
  * @param      None
  * @retval     None
  *****************************************************************************/
void Systick_Config(void);

/******************************************************************************
  * @function   ADC_Config
  * @brief      ADC configuration.
  * @param      None
  * @retval     None
  *****************************************************************************/
void ADC_Config(void);

/******************************************************************************
  * @function   DMA_Config
  * @brief      DMA configuration.
  * @param      None
  * @retval     None
  *****************************************************************************/
void DMA_Config(void);

/******************************************************************************
  * @function   TIM15_Config
  * @brief      TIM15 configuration for trigger the DMA / ADC1.
  * @param      None
  * @retval     None
  *****************************************************************************/
void TIM15_Config(void);

/******************************************************************************
  * @function   PMB_I2C_Config
  * @brief      I2C (PMBus) and PMB control pin configuration.
  * @param      None
  * @retval     None
  *****************************************************************************/
void PMB_I2C_Config(void);

/******************************************************************************
  * @function   EN_And_PG_Signals_Config
  * @brief      Inits pins for enable (EN) and power good (PG) signals.
  * @param      None
  * @retval     None
  *****************************************************************************/
void EN_And_PG_Signals_Config(void);

/******************************************************************************
  * @function   EXTI_Config
  * @brief      Inits external interrupts.
  * @param      None
  * @retval     None
  *****************************************************************************/
void EXTI_Config(void);

/******************************************************************************
  * @function   TIM_Modulation_Config
  * @brief      Setup timers to generate frequencies for spread spectrum
  * 			for the regulator switching frequencies.
  * @param      None
  * @retval     None
  *****************************************************************************/
void TIM_Modulation_Config(void);

/******************************************************************************
  * @function   Fan_Control_Config
  * @brief      Inits pins and timers for fan control (PWM and freq. measurement).
  * @param      None
  * @retval     None
  *****************************************************************************/
void Fan_Control_Config(void);

/******************************************************************************
  * @function   Delay
  * @brief      Inserts a delay time.
  * @param      nTime: specifies the delay time length, in miliseconds.
  * @retval     None
  *****************************************************************************/
void Delay(volatile uint32_t nTime);

/******************************************************************************
  * @function   TimingDelay_Decrement
  * @brief      Decrements the TimingDelay variable.
  * @param      None
  * @retval     None
  *****************************************************************************/
void TimingDelay_Decrement(void);

/******************************************************************************
  * @function   Count_Average
  * @brief      Counts average value.
  * @param      value: input array - average is calculated with its data
  * @retval     average value
  *****************************************************************************/
uint16_t Count_Average(const uint16_t value[]);

/******************************************************************************
  * @function   LED_Reset_Blinky
  * @brief      Toggles the leds after the reset.
  * @param      None
  * @retval     None
  *****************************************************************************/
void LED_Reset_Blinky(void);

/******************************************************************************
  * @function   Wait_For_I2C_Flag
  * @brief      Checks I2C status + handels timeout.
  * @param      flag: I2C flag name to be checked
  * @retval     None.
  *****************************************************************************/
void Wait_For_I2C_Flag(uint32_t flag);

#endif /* __HW_CONFIG_H */
