/**
 ******************************************************************************
 * @file    power_monitor.c
 * @author  J. Kovanda & T. Rykl & Z. Kos
 * @date    10-July-2014
 * @brief   Functions for power regulator monitoring
 ******************************************************************************
 ******************************************************************************
 **/

/* Includes ------------------------------------------------------------------*/
#include "hw_config.h"
#include "project_config.h"
#include "serial.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
volatile uint8_t PG_DownFlag_PB3 = 0u; // PG pin goes to 0 -> flag is set to 1 (in interrupt)
volatile uint8_t PG_DownFlag_PC10 = 0u; // PG pin goes to 0 -> flag is set to 1 (in interrupt)
volatile uint8_t PG_DownFlag_PB11 = 0u; // PG pin goes to 0 -> flag is set to 1 (in interrupt)
volatile uint8_t PG_DownFlag_PB13 = 0u; // PG pin goes to 0 -> flag is set to 1 (in interrupt)
volatile uint8_t PG_DownFlag_PB15 = 0u; // PG pin goes to 0 -> flag is set to 1 (in interrupt)
volatile uint8_t PMB_ControlResetFlag = 0u; // if PMB_CNTRL_PIN goes to 0 -> flag is set to 1 (in interrupt)

/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/

/******************************************************************************
 * @function   Check_Signal_Stability
 * @brief      Checks I2C status + handels timeout.
 * @param      GPIOx: where x can be (A, B, C, D, E or F) to select the GPIO peripheral
 * @param		GPIO_Pin: specifies the port bit to read
 * @retval     Stability status: 0u - signal is stable zero; 1u - signal is not stable.
 *****************************************************************************/
static uint8_t Check_Signal_Stability(GPIO_TypeDef* GPIOx, uint16_t GPIO_Pin)
{
    uint8_t value1;
    uint8_t value2;

    value1 = GPIO_ReadInputDataBit(GPIOx, GPIO_Pin);
    Delay(2);
    value2 = GPIO_ReadInputDataBit(GPIOx, GPIO_Pin);

    if (value1 == 0u && value2 == 0u)
    {
	return 0u; /* signal is stable zero */
    }
    else
    {
	return 1u; /* signal is not stable */
    }
}

/******************************************************************************
 * @function   Start_Regulators
 * @brief      Power up sequence - checks PMB_CNTRL pin and starts regulators.
 * 			It checks their PG pins.
 * @param      None.
 * @retval     None.
 *****************************************************************************/
void Start_Regulators(void)
{	
    write_line("Waiting for PMB_CNTRL\r\n");
    /* Wait for PMB_CNTRL signal */
    while((GPIO_ReadInputDataBit(GPIOB, PMB_CNTRL_PIN)) != 1u) {}

    /* *
     * Power sequence:
     * 1) main 5V regulator
     * 2) 1.05V regulator
     * 3) 3.3V regulator for CPU-IO
     * 4) 2.5V regulator
     * 5) 3.3V regulator for PCIe
     *
     * */

    /* switch on main 5V regulator */
    GPIO_SetBits(GPIOB, ENABLE_PIN_5V_MAIN_REG);
    /* check PG */
    while((GPIO_ReadInputDataBit(GPIOB, PG_PIN_5V_MAIN_REG))    != 1u) {}

    /* switch on 1V05 regulator */
    GPIO_SetBits(GPIOB, ENABLE_PIN_1V05_REG);
    /* Check PG */
    while((GPIO_ReadInputDataBit(GPIOC, PG_PIN_1V05_REG))       != 1u) {}

    /* switch on 3V3 CPU / IO regulator */
    GPIO_SetBits(GPIOB, ENABLE_PIN_3V3_CPU_IO_REG);

    /* switch on 2V5 regulator */
    GPIO_SetBits(GPIOB, ENABLE_PIN_2V5_REG);

    /* switch on 3V3 PCI regulator */
    GPIO_SetBits(GPIOB, ENABLE_PIN_3V3_PCI_REG);


    /* Wait for all PG signals */
    while((GPIO_ReadInputDataBit(GPIOB, PG_PIN_3V3_CPU_IO_REG)) != 1u) {}
    while((GPIO_ReadInputDataBit(GPIOB, PG_PIN_2V5_REG)) 	!= 1u) {}
    while((GPIO_ReadInputDataBit(GPIOB, PG_PIN_3V3_PCI_REG)) 	!= 1u) {}


    /* Output power good pin set to 1 */
    GPIO_SetBits(GPIOC, PWRGOOD_PIN);
    write_line("Regulators running PWRGOOD_PIN set to 1\r\n");
}

void reset_all_power_signals(void)
{
    write_line("Reset all power signals\r\n");
    /* Output power good pin set to 0 */
    GPIO_ResetBits(GPIOC, PWRGOOD_PIN);
    /* Disable all regulators */
    GPIO_ResetBits(GPIOB, ENABLE_PIN_5V_MAIN_REG);
    GPIO_ResetBits(GPIOB, ENABLE_PIN_1V05_REG);
    GPIO_ResetBits(GPIOB, ENABLE_PIN_3V3_PCI_REG);
    GPIO_ResetBits(GPIOB, ENABLE_PIN_2V5_REG);
    GPIO_ResetBits(GPIOB, ENABLE_PIN_3V3_CPU_IO_REG);
}

/******************************************************************************
 * @function   Power_Monitor
 * @brief      Monitors the power good signals (via interrupts) and manages next
 * 			reactions.
 * 			 - Checks PMB_CNTRL pin -> SW reset.
 * 			 - Checks PG signals -> disable regulators
 * @param      None.
 * @retval     None.
 *****************************************************************************/
void Power_Monitor(void)
{
    uint8_t pin_state_PB3 = 1u;
    uint8_t pin_state_PC10 = 1u;
    uint8_t pin_state_PB11 = 1u;
    uint8_t pin_state_PB13 = 1u;
    uint8_t pin_state_PB15 = 1u;

    if (PG_DownFlag_PB3 == 1u)
    {
	pin_state_PB3 = Check_Signal_Stability(GPIOB, PG_PIN_5V_MAIN_REG);
	PG_DownFlag_PB3 = 0u;
	write_line("PB3 interrupt\r\n");
    }

    if (PG_DownFlag_PC10 == 1u)
    {
	pin_state_PC10 = Check_Signal_Stability(GPIOC, PG_PIN_1V05_REG);
	PG_DownFlag_PC10 = 0u;
	write_line("PC10 interrupt\r\n");
    }

    if (PG_DownFlag_PB11 == 1u)
    {
	pin_state_PB11 = Check_Signal_Stability(GPIOB, PG_PIN_3V3_CPU_IO_REG);
	PG_DownFlag_PB11 = 0u;
	write_line("PB11 interrupt\r\n");
    }

    if (PG_DownFlag_PB13 == 1u)
    {
	pin_state_PB13 = Check_Signal_Stability(GPIOB, PG_PIN_3V3_PCI_REG);
	PG_DownFlag_PB13 = 0u;
	write_line("PB13 interrupt\r\n");
    }

    if (PG_DownFlag_PB15 == 1u)
    {
	pin_state_PB15 = Check_Signal_Stability(GPIOB, PG_PIN_2V5_REG);
	PG_DownFlag_PB15 = 0u;
	write_line("PB15 interrupt\r\n");
    }

    if ( (pin_state_PB3 == 0u) || (pin_state_PC10 == 0u) || (pin_state_PB11 == 0u)
	    || (pin_state_PB13 == 0u) || (pin_state_PB15 == 0u))
    {
	write_line("Reseting\r\n");
	reset_all_power_signals();
	Delay(50);
	NVIC_SystemReset();
    }

    if (PMB_ControlResetFlag == 1u)
    {
	PMB_ControlResetFlag = 0u;
	write_line("PMB CTRL interrupt\r\n");

        write_line("Resetting\r\n");
        reset_all_power_signals();
        Delay(50);
	/* SW reset */
        NVIC_SystemReset();
    }
}
