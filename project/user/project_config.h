/**
  ******************************************************************************
  * @file    project_config.h
  * @author  J. Kovanda & T. Rykl & Z. Kos
  * @date    3-September-2014
  * @brief   Project specific definitions and macros, variables,...
  ******************************************************************************
  ******************************************************************************
**/

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __PROJECT_CONFIG_H
#define __PROJECT_CONFIG_H

#include "stm32f0xx.h"

/* Private typedef -----------------------------------------------------------*/

typedef struct
{
	uint16_t voltage;
	uint16_t current;
	int16_t temperature;
} sMeasureGroup;


typedef enum
{
	Low = 0,
	Middle = 1,
	High = 2,
} eTemperatureState;

typedef enum
{
    NoDivider = 0,
    Divider = 1
} eRegulatorType;

typedef enum
{
	NoError = 0,
	Timeout = 1
} eI2CBusState;

/* Private define ------------------------------------------------------------*/

#define DMA_ADC_TRIGGERED_BY_TIM
//#define TACHO_EXTERNAL_INT

#define DMA_BUFFER_SIZE						13u
#define AVERAGE_BUFFER_SIZE					8u

#define GPIOA_PERIPH_CLOCK					RCC_AHBPeriph_GPIOA
#define GPIOB_PERIPH_CLOCK					RCC_AHBPeriph_GPIOB
#define GPIOC_PERIPH_CLOCK					RCC_AHBPeriph_GPIOC
#define GPIOD_PERIPH_CLOCK					RCC_AHBPeriph_GPIOD

/* PWM fan control defines ****************************************************/
#define	TEMPERATURE_LOW_LIMIT				60 // �C
#define TEMPERATURE_HIGH_LIMIT				100 // �C
#define PWM_TIMER_PERIOD					1000
#define PWM_CONSTANT						2 // increase by 1�C means increase "toggle time" by 2% of timer period
#define PWM_TIMER_PERIOD_ONE_PERCENT		10
#define PWM_TIMER_PERIOD_LOW_LIMIT			100 //10% of period
#define PWM_TIMER_PERIOD_HIGH_LIMIT			900 //90% of period
#define TEMP_HYSTERESIS						5 // 5�C
#define SET_FAN_PWM_PIN 					(GPIOD->BSRR = PWR_FAN_PWM_PIN) // set pin to logic 1
#define RESET_FAN_PWM_PIN 					(GPIOD->BRR = PWR_FAN_PWM_PIN)	// seti pin to 0

#define TIMER_PERIOD						600u // ms - if Fan control is used, set the correct timer period


/* Measuring defines *********************************************************/
#define STM32_VREF										3300 // 3.3 V
#define CURRENT_FACTOR									1000 // factor for current measurement, current in mA
#define MAX9934_GAIN									25u	// 25 uA/mV
#define R_OUT											5 	// 5 kOhm
#define SHUNT_VALUE										2u 	// 2 mOhm
#define CALIBRATION_VOLTAGE_FACTOR						1020u 	// 2% -> value * 1.02 * 1000 = value * 1020
#define CALIBRATION_CURRENT_FACTOR_LOW_CURRENT			1400u 	// 40%
#define CALIBRATION_CURRENT_FACTOR_MIDD_CURRENT			1200u 	// 20%
#define CALIBRATION_CURRENT_FACTOR_HIGH_CURRENT			1050u 	// 5%
#define CALIBRATION_MULTIPLY_FACTOR						1000u
#define LOW_CURRENT										700u 	// 700mA
#define MIDD_CURRENT									2500u 	// 2500mA
//#define HIGH_CURRENT									3000u 	// 3000mA


/* ADC ***********************************************************************/
#define ADC1_DR_Address    					0x40012440u
#define ADC_SAMPLE_TIME						ADC_SampleTime_239_5Cycles
#define ADC_PERIPH_CLOCK					RCC_APB2Periph_ADC1

#define GPIO_TEMPERATURE_PIN_1V05			GPIO_Pin_0	//GPIOA
#define GPIO_VOLTAGE_PIN_1V05				GPIO_Pin_1	//GPIOA
#define GPIO_CURRENT_PIN_1V05				GPIO_Pin_2	//GPIOA

#define GPIO_TEMPERATURE_PIN_3V3_CPU_IO		GPIO_Pin_3	//GPIOA
#define GPIO_VOLTAGE_PIN_3V3_CPU_IO			GPIO_Pin_4	//GPIOA
#define GPIO_CURRENT_PIN_3V3_CPU_IO			GPIO_Pin_5	//GPIOA

#define GPIO_TEMPERATURE_PIN_3V3_PCI		GPIO_Pin_1 // GPIOB !
#define GPIO_VOLTAGE_PIN_3V3_PCI			GPIO_Pin_6 //GPIOA
#define GPIO_CURRENT_PIN_3V3_PCI			GPIO_Pin_7 //GPIOA

#define GPIO_TEMPERATURE_PIN_5V_MAIN		GPIO_Pin_0 // GPIOB !
#define GPIO_VOLTAGE_PIN_5V_MAIN			GPIO_Pin_4 // GPIOC !
#define GPIO_CURRENT_PIN_5V_MAIN			GPIO_Pin_5 // GPIOC !
/*
#define GPIO_TEMPERATURE_PIN_1V5_PCI		GPIO_Pin_1 // GPIOC !
#define GPIO_VOLTAGE_PIN_1V5_PCI			GPIO_Pin_2 // GPIOC !
#define GPIO_CURRENT_PIN_1V5_PCI			GPIO_Pin_3 // GPIOC !
*/
/* I2C - PMBus ****************************************************************/
#define PMB_I2C_PERIPH_NAME					I2C1
#define PMB_I2C_PERIPH_CLOCK				RCC_APB1Periph_I2C1
#define PMB_DATA_PIN						GPIO_Pin_7 // I2C1_SDA - GPIOB
#define PMB_CLK_PIN							GPIO_Pin_6 // I2C1_SCL - GPIOB

#define PMB_ALERT_PIN						GPIO_Pin_5 // GPIOB
#define PMB_CNTRL_PIN						GPIO_Pin_8 // GPIOB

#define PMB_I2C_SDA_SOURCE              	GPIO_PinSource7
#define PMB_I2C_SCL_SOURCE              	GPIO_PinSource6

#define I2C1_ALTERNATE_FUNCTION             GPIO_AF_1
#define I2C_TIMING     						0x1045061D

/* Enable signals for regulators *********************************************/
#define ENABLE_PIN_5V_MAIN_REG				GPIO_Pin_2 // GPIOB
#define ENABLE_PIN_1V05_REG					GPIO_Pin_4 // GPIOB
#define ENABLE_PIN_3V3_CPU_IO_REG			GPIO_Pin_10 // GPIOB
#define ENABLE_PIN_3V3_PCI_REG				GPIO_Pin_12 // GPIOB
#define ENABLE_PIN_2V5_REG					GPIO_Pin_14 // GPIOB

/* Power good signals from regulators ****************************************/
#define PG_PIN_5V_MAIN_REG					GPIO_Pin_3 // GPIOB
#define PG_PIN_3V3_CPU_IO_REG				GPIO_Pin_11 // GPIOB
#define PG_PIN_3V3_PCI_REG					GPIO_Pin_13 // GPIOB
#define PG_PIN_2V5_REG						GPIO_Pin_15 // GPIOB
#define PG_PIN_1V05_REG						GPIO_Pin_10 // GPIOC!

/* Power good output signal  *************************************************/
#define PWRGOOD_PIN							GPIO_Pin_12 // GPIOC

/* Fan control signal  *******************************************************/
#define PWR_FAN_TACHO_PIN					GPIO_Pin_2  // GPIOD
#define PWR_FAN_PWM_PIN						GPIO_Pin_11 // GPIOC

#endif /* __PROJECT_CONFIG_H */
