/**
  ******************************************************************************
  * @file    hw_config.c
  * @author  J. Kovanda & T. Rykl & Z. Kos
  * @date    18-April-2014
  * @brief   HW specific functions (interface inits, peripheral settings,...etc)
  ******************************************************************************
  ******************************************************************************
**/

/* Includes ------------------------------------------------------------------*/
#include "project_config.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
volatile uint32_t TimingDelay; // global variable time delay
volatile eI2CBusState I2Cstatus = NoError; // 0 - no error; 1 - error (timeout)
volatile const uint16_t ConvertedData_Tab[]; // measured data from ADC
/* capture/compare values for settings the slightly different frequencies */
volatile uint16_t CCR1_Val = 2300;
volatile uint16_t CCR2_Val = 2400;
volatile uint16_t CCR3_Val = 2480;
volatile uint16_t CCR4_Val = 2560;
volatile uint16_t CCR5_Val = 2250;

/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/

/*******************************************************************************
  * @function   Systick_Config
  * @brief      Setup SysTick Timer for 1 msec interrupts.
  * @param      None
  * @retval     None
  *****************************************************************************/
void Systick_Config(void)
{
	if (SysTick_Config(SystemCoreClock / 1000u))
	 {
	   /* Capture error */
	   while (1);
	 }
}

/******************************************************************************
  * @function   ADC_Config
  * @brief      ADC configuration.
  * @param      None
  * @retval     None
  *****************************************************************************/
void ADC_Config(void)
{
	ADC_InitTypeDef     ADC_InitStructure;
	GPIO_InitTypeDef    GPIO_InitStructure;
	  /* ADC1 DeInit */
	ADC_DeInit(ADC1);

	  /* GPIOA Periph clock enable */
	RCC_AHBPeriphClockCmd(GPIOA_PERIPH_CLOCK | GPIOB_PERIPH_CLOCK, ENABLE);

	   /* ADC1 Periph clock enable */
	RCC_APB2PeriphClockCmd(ADC_PERIPH_CLOCK, ENABLE);

	ADC_Cmd(ADC1, DISABLE);

	  /* Configure ADC channels as analog input */
	GPIO_InitStructure.GPIO_Pin = GPIO_TEMPERATURE_PIN_1V05 | GPIO_VOLTAGE_PIN_1V05 |
			  GPIO_CURRENT_PIN_1V05 | GPIO_TEMPERATURE_PIN_3V3_CPU_IO | GPIO_VOLTAGE_PIN_3V3_CPU_IO |
			  GPIO_CURRENT_PIN_3V3_CPU_IO | GPIO_VOLTAGE_PIN_3V3_PCI | GPIO_CURRENT_PIN_3V3_PCI;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AN;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_Init(GPIOA, &GPIO_InitStructure);

	GPIO_InitStructure.GPIO_Pin = GPIO_TEMPERATURE_PIN_3V3_PCI | GPIO_TEMPERATURE_PIN_5V_MAIN;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AN;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_Init(GPIOB, &GPIO_InitStructure);

	GPIO_InitStructure.GPIO_Pin = GPIO_VOLTAGE_PIN_5V_MAIN | GPIO_CURRENT_PIN_5V_MAIN;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AN;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_Init(GPIOC, &GPIO_InitStructure);

	  /* Initialize ADC structure */
	ADC_StructInit(&ADC_InitStructure);

	/* Configure the ADC1 in continuous mode with a resolution equal to 12 bits  */
	ADC_InitStructure.ADC_Resolution = ADC_Resolution_12b;
	ADC_InitStructure.ADC_ContinuousConvMode = DISABLE;
	ADC_InitStructure.ADC_ExternalTrigConvEdge = ADC_ExternalTrigConvEdge_Rising;
	ADC_InitStructure.ADC_ExternalTrigConv = ADC_ExternalTrigConv_T15_TRGO;
	//ADC_InitStructure.ADC_ContinuousConvMode = ENABLE;
	//ADC_InitStructure.ADC_ExternalTrigConvEdge = ADC_ExternalTrigConvEdge_None;
	ADC_InitStructure.ADC_DataAlign = ADC_DataAlign_Right;
	ADC_InitStructure.ADC_ScanDirection = ADC_ScanDirection_Upward;

	ADC_Init(ADC1, &ADC_InitStructure);

	  /* Convert the ADC1 Channel0 with 55.5 Cycles as sampling time */
	ADC_ChannelConfig(ADC1, ADC_Channel_0 , ADC_SAMPLE_TIME);

	  /* Convert the ADC1 Channel1 with 55.5 Cycles as sampling time */
	ADC_ChannelConfig(ADC1, ADC_Channel_1 , ADC_SAMPLE_TIME);

	  /* Convert the ADC1 Channel2 with 55.5 Cycles as sampling time */
	ADC_ChannelConfig(ADC1, ADC_Channel_2 , ADC_SAMPLE_TIME);

	  /* Convert the ADC1 Channel3 with 55.5 Cycles as sampling time */
	ADC_ChannelConfig(ADC1, ADC_Channel_3 , ADC_SAMPLE_TIME);

	  /* Convert the ADC1 Channel4 with 55.5 Cycles as sampling time */
	ADC_ChannelConfig(ADC1, ADC_Channel_4 , ADC_SAMPLE_TIME);

	  /* Convert the ADC1 Channel5 with 55.5 Cycles as sampling time */
	ADC_ChannelConfig(ADC1, ADC_Channel_5 , ADC_SAMPLE_TIME);

	  /* Convert the ADC1 Channel6 with 55.5 Cycles as sampling time */
	ADC_ChannelConfig(ADC1, ADC_Channel_6 , ADC_SAMPLE_TIME);

	  /* Convert the ADC1 Channel7 with 55.5 Cycles as sampling time */
	ADC_ChannelConfig(ADC1, ADC_Channel_7 , ADC_SAMPLE_TIME);

	  /* Convert the ADC1 Channel8 with 55.5 Cycles as sampling time */
	ADC_ChannelConfig(ADC1, ADC_Channel_8 , ADC_SAMPLE_TIME);

	  /* Convert the ADC1 Channel9 with 55.5 Cycles as sampling time */
	ADC_ChannelConfig(ADC1, ADC_Channel_9 , ADC_SAMPLE_TIME);

	  /* Convert the ADC1 Channel9 with 55.5 Cycles as sampling time */
	ADC_ChannelConfig(ADC1, ADC_Channel_14 , ADC_SAMPLE_TIME);

	  /* Convert the ADC1 Channel9 with 55.5 Cycles as sampling time */
	ADC_ChannelConfig(ADC1, ADC_Channel_15 , ADC_SAMPLE_TIME);

	  /* Convert the ADC1 temperature sensor  with 55.5 Cycles as sampling time */
	ADC_ChannelConfig(ADC1, ADC_Channel_TempSensor , ADC_SAMPLE_TIME);
	ADC_TempSensorCmd(ENABLE);


	  /* ADC Calibration */
	ADC_GetCalibrationFactor(ADC1);

	  /* ADC DMA request in circular mode */
	ADC_DMARequestModeConfig(ADC1, ADC_DMAMode_Circular);

	  /* Enable ADC_DMA */
	ADC_DMACmd(ADC1, ENABLE);

	  /* Enable the ADC peripheral */
	ADC_Cmd(ADC1, ENABLE);

	  /* Wait the ADRDY flag */
	while(!ADC_GetFlagStatus(ADC1, ADC_FLAG_ADRDY));


	  /* ADC1 regular Software Start Conv */
	ADC_StartOfConversion(ADC1);
}

/******************************************************************************
  * @function   DMA_Config
  * @brief      DMA configuration.
  * @param      None
  * @retval     None
  *****************************************************************************/
void DMA_Config(void)
{
	DMA_InitTypeDef   DMA_InitStructure;
	NVIC_InitTypeDef NVIC_InitStructure;

	  /* DMA1 clock enable */
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_DMA1 , ENABLE);

	  /* DMA1 Channel1 Config */
	DMA_DeInit(DMA1_Channel1);
	DMA_InitStructure.DMA_PeripheralBaseAddr = (uint32_t)ADC1_DR_Address;
	DMA_InitStructure.DMA_MemoryBaseAddr = (uint32_t)ConvertedData_Tab;
	DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralSRC;
	DMA_InitStructure.DMA_BufferSize = DMA_BUFFER_SIZE;
	DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
	DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;
	DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_HalfWord;
	DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_HalfWord;
	DMA_InitStructure.DMA_Mode = DMA_Mode_Circular;
	DMA_InitStructure.DMA_Priority = DMA_Priority_High;
	DMA_InitStructure.DMA_M2M = DMA_M2M_Disable;
	DMA_Init(DMA1_Channel1, &DMA_InitStructure);


	  /* DMA1 Channel1 enable */
	DMA_Cmd(DMA1_Channel1, ENABLE);

	  /* Enable DMA1 Channel1 Transfer Complete interrupt */
	DMA_ITConfig(DMA1_Channel1, DMA_IT_TC | DMA_IT_TE, ENABLE);

	  /* Enable DMA1 channel1 IRQ Channel */
	NVIC_InitStructure.NVIC_IRQChannel = DMA1_Channel1_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPriority = 0x02;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);

}

/******************************************************************************
  * @function   TIM15_Config
  * @brief      TIM15 configuration for trigger the DMA / ADC1.
  * @param      None
  * @retval     None
  *****************************************************************************/
void TIM15_Config(void)
{
#ifdef DMA_ADC_TRIGGERED_BY_TIM

	TIM_TimeBaseInitTypeDef TIM_TimeBaseStructure;

	RCC_APB2PeriphClockCmd(RCC_APB2Periph_TIM15, ENABLE);

	/* Time base configuration */
	TIM_TimeBaseStructInit(&TIM_TimeBaseStructure);
	TIM_TimeBaseStructure.TIM_Period = 120;
	TIM_TimeBaseStructure.TIM_Prescaler = ((SystemCoreClock/1200) - 1);
	TIM_TimeBaseStructure.TIM_ClockDivision = 0;
	TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
	TIM_TimeBaseInit(TIM15, &TIM_TimeBaseStructure);

	/* TIM15 TRGO selection */
	TIM_SelectOutputTrigger(TIM15, TIM_TRGOSource_Update); // ADC_ExternalTrigConv_T15_TRGO

//	TIM_DMACmd(TIM15, TIM_DMA_Update, ENABLE);

	/* TIM15 enable counter */
	TIM_Cmd(TIM15, ENABLE);

#endif
}

/******************************************************************************
  * @function   PMB_I2C_Config
  * @brief      I2C (PMBus) and PMB control pin configuration.
  * @param      None
  * @retval     None
  *****************************************************************************/
void PMB_I2C_Config(void)
{
	GPIO_InitTypeDef  GPIO_InitStructure;
	I2C_InitTypeDef  I2C_InitStructure;
	NVIC_InitTypeDef NVIC_InitStructure;

	  /* Configure the I2C clock source. The clock is derived from the Sysclk */
	RCC_I2CCLKConfig(RCC_I2C1CLK_SYSCLK);
	
	  /* I2C Periph clock enable */
	RCC_APB1PeriphClockCmd(PMB_I2C_PERIPH_CLOCK, ENABLE);

	RCC_AHBPeriphClockCmd(GPIOB_PERIPH_CLOCK, ENABLE);

	  /* Connect PXx to I2C_SCL */
	GPIO_PinAFConfig(GPIOB, PMB_I2C_SCL_SOURCE, I2C1_ALTERNATE_FUNCTION);

	  /* Connect PXx to I2C_SDA */
	GPIO_PinAFConfig(GPIOB, PMB_I2C_SDA_SOURCE, I2C1_ALTERNATE_FUNCTION);

	  /* Configure I2C pins: SCL */
	GPIO_InitStructure.GPIO_Pin = PMB_CLK_PIN;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
	GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_NOPULL;
	GPIO_Init(GPIOB, &GPIO_InitStructure);

	  /* Configure I2C pins: SDA */
	GPIO_InitStructure.GPIO_Pin = PMB_DATA_PIN;
	GPIO_Init(GPIOB, &GPIO_InitStructure);

	  /* I2C configuration */
	I2C_InitStructure.I2C_Mode = I2C_Mode_I2C;
	I2C_InitStructure.I2C_AnalogFilter = I2C_AnalogFilter_Enable;
	I2C_InitStructure.I2C_DigitalFilter = 0x00;
	I2C_InitStructure.I2C_OwnAddress1 = 0x55;			// own address of STM32 - slave mode
	I2C_InitStructure.I2C_Ack = I2C_Ack_Enable;
	I2C_InitStructure.I2C_AcknowledgedAddress = I2C_AcknowledgedAddress_7bit;
	I2C_InitStructure.I2C_Timing = I2C_TIMING;

	  /* Apply I2C configuration after enabling it */
	I2C_Init(PMB_I2C_PERIPH_NAME, &I2C_InitStructure);

	/* Address match and data received interrupt*/
	I2C_ITConfig(PMB_I2C_PERIPH_NAME, I2C_IT_ADDRI | I2C_IT_RXI, ENABLE);

	  /* I2C Peripheral Enable */
	I2C_Cmd(PMB_I2C_PERIPH_NAME, ENABLE);

	NVIC_InitStructure.NVIC_IRQChannel = I2C1_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPriority = 0x01;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);

	/* PMB Alert and PMB Control pins config *****************************/

		  /* PMB_ALERTn */
/*
	GPIO_InitStructure.GPIO_Pin = PMB_ALERT_PIN;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_Level_2;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
	GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_NOPULL;
	GPIO_Init(GPIOB, &GPIO_InitStructure);
	*/

		/* PMB_CNTRL */
	GPIO_InitStructure.GPIO_Pin = PMB_CNTRL_PIN;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN;
	GPIO_Init(GPIOB, &GPIO_InitStructure);

}


/******************************************************************************
  * @function   EN_And_PG_Signals_Config
  * @brief      Inits pins for enable (EN) and power good (PG) signals.
  * @param      None
  * @retval     None
  *****************************************************************************/
void EN_And_PG_Signals_Config(void)
{
	GPIO_InitTypeDef  GPIO_InitStructure;

	  /* GPIOC Periph clock enable */
	RCC_AHBPeriphClockCmd(GPIOB_PERIPH_CLOCK | GPIOC_PERIPH_CLOCK, ENABLE);

	/* Enable signals */
	GPIO_InitStructure.GPIO_Pin = ENABLE_PIN_5V_MAIN_REG | ENABLE_PIN_1V05_REG |
			ENABLE_PIN_3V3_CPU_IO_REG | ENABLE_PIN_3V3_PCI_REG | ENABLE_PIN_2V5_REG;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_Level_2;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_Init(GPIOB, &GPIO_InitStructure);

	/* Power good signals */
	GPIO_InitStructure.GPIO_Pin = PG_PIN_5V_MAIN_REG | PG_PIN_3V3_CPU_IO_REG |
			PG_PIN_3V3_PCI_REG | PG_PIN_2V5_REG;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_Init(GPIOB, &GPIO_InitStructure);

	GPIO_InitStructure.GPIO_Pin = PG_PIN_1V05_REG;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_Init(GPIOC, &GPIO_InitStructure);

	/* Main power good signal */
	GPIO_InitStructure.GPIO_Pin = PWRGOOD_PIN;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_Level_2;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_Init(GPIOC, &GPIO_InitStructure);

    /* PMB_CNTRL */
    GPIO_InitStructure.GPIO_Pin = PMB_CNTRL_PIN;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
    GPIO_Init(GPIOB, &GPIO_InitStructure);



    /* PMB_DATA_PIN */
    GPIO_InitStructure.GPIO_Pin = PMB_DATA_PIN;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
    GPIO_Init(GPIOB, &GPIO_InitStructure);

    /* PMB_CLK_PIN */
    GPIO_InitStructure.GPIO_Pin = PMB_CLK_PIN;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
    GPIO_Init(GPIOB, &GPIO_InitStructure);

	/* PMB_ALERTn */
	GPIO_InitStructure.GPIO_Pin = PMB_ALERT_PIN;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_Level_2;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_NOPULL;
	GPIO_Init(GPIOB, &GPIO_InitStructure);


	GPIO_ResetBits(GPIOB, PMB_ALERT_PIN);

	GPIO_ResetBits(GPIOC, PWRGOOD_PIN);
}

/******************************************************************************
  * @function   Fan_Control_Config
  * @brief      Inits pins and timers for fan control (PWM and freq. measurement).
  * @param      None
  * @retval     None
  *****************************************************************************/
void Fan_Control_Config(void)
{
	GPIO_InitTypeDef  GPIO_InitStructure;
	TIM_TimeBaseInitTypeDef TIM_TimeBaseStructure;
	NVIC_InitTypeDef NVIC_InitStructure;
	TIM_OCInitTypeDef  TIM_OCInitStructure;

	/* Pins configuration ****************************************************/
	/* GPIOC and GPIOD Periph clock enable */
	RCC_AHBPeriphClockCmd(GPIOC_PERIPH_CLOCK | GPIOD_PERIPH_CLOCK, ENABLE);

	/* Output "PWM" signal */
	GPIO_InitStructure.GPIO_Pin = PWR_FAN_PWM_PIN;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_Level_3;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_Init(GPIOC, &GPIO_InitStructure);

	/* input tacho signal */
	GPIO_InitStructure.GPIO_Pin = PWR_FAN_TACHO_PIN;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN;
	GPIO_Init(GPIOD, &GPIO_InitStructure);

	/* Timer 14 config ******************************************************/
	/* time base for the pulses counter from tacho signal *******************/
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM14, ENABLE);

    //If used, set the timer correctly - not used / not tested

	/* Time base configuration */
	TIM_TimeBaseStructInit(&TIM_TimeBaseStructure);
	TIM_TimeBaseStructure.TIM_Period = 800;
	TIM_TimeBaseStructure.TIM_Prescaler = ((SystemCoreClock/1200) - 1);
	TIM_TimeBaseStructure.TIM_ClockDivision = 0;
	TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
	TIM_TimeBaseInit(TIM14, &TIM_TimeBaseStructure);

	/* TIM14 enable counter */
	TIM_Cmd(TIM14, ENABLE);

	/* Immediate load of TIM14 Prescaler value */
	//TIM_PrescalerConfig(TIM14, ((SystemCoreClock/1200) - 1), TIM_PSCReloadMode_Immediate);

	/* Clear TIM14 update pending flag */
	TIM_ClearFlag(TIM14, TIM_FLAG_Update);

	/* Enable TIM14 Update and CC1 interrupt */
	TIM_ITConfig(TIM14, TIM_IT_Update, ENABLE);

	/* Enable and set TIM14 Interrupt */
	NVIC_InitStructure.NVIC_IRQChannel = TIM14_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPriority = 0x03;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
	/************************************************************************/

	/* Timer 16 config ******************************************************/
	/* time base for PWM ****************************************************/
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_TIM16, ENABLE);

	/* Time base configuration */
	TIM_TimeBaseStructInit(&TIM_TimeBaseStructure);
	TIM_TimeBaseStructure.TIM_Period = PWM_TIMER_PERIOD;
	TIM_TimeBaseStructure.TIM_Prescaler = ((SystemCoreClock/1200) - 1);
	TIM_TimeBaseStructure.TIM_ClockDivision = 0;
	TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
	TIM_TimeBaseInit(TIM16, &TIM_TimeBaseStructure);

	 /* Output Compare Timing Mode configuration: Channel1 */
	TIM_OCStructInit(&TIM_OCInitStructure);
	TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_Timing;
	TIM_OCInitStructure.TIM_Pulse = 600;
	TIM_OC1Init(TIM16, &TIM_OCInitStructure);

	/* TIM16 enable counter */
	TIM_Cmd(TIM16, ENABLE);

	/* Clear TIM16 update pending flag */
	TIM_ClearFlag(TIM16, TIM_FLAG_Update | TIM_FLAG_CC1);

	/* Enable TIM16 Update and CC1 interrupt */
	TIM_ITConfig(TIM16, TIM_IT_CC1 | TIM_IT_Update, ENABLE);

	/* Enable the TIM16 Interrupt */
	NVIC_InitStructure.NVIC_IRQChannel = TIM16_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPriority = 0x03;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);

	/* set output to 1 */
	GPIO_SetBits(GPIOC, PWR_FAN_PWM_PIN);
}

/******************************************************************************
  * @function   EXTI_Config
  * @brief      Inits external interrupts.
  * @param      None
  * @retval     None
  *****************************************************************************/
void EXTI_Config(void)
{
	EXTI_InitTypeDef   EXTI_InitStructure;
	NVIC_InitTypeDef NVIC_InitStructure;

	/* Enable SYSCFG clock */
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_SYSCFG, ENABLE);

	/* Enable GPIOC clock */
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOC | RCC_AHBPeriph_GPIOB | RCC_AHBPeriph_GPIOD, ENABLE);

	/* Connect EXTI3 Line to PB3 pin */
	SYSCFG_EXTILineConfig(EXTI_PortSourceGPIOB, EXTI_PinSource3);

	/* Configure EXTI3 line */
	EXTI_InitStructure.EXTI_Line = EXTI_Line3;
	EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
	EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Falling;
	EXTI_InitStructure.EXTI_LineCmd = ENABLE;
	EXTI_Init(&EXTI_InitStructure);

	/* Connect EXTI11 Line to PB11 pin */
	SYSCFG_EXTILineConfig(EXTI_PortSourceGPIOB, EXTI_PinSource11);

	/* Configure EXTI11 line */
	EXTI_InitStructure.EXTI_Line = EXTI_Line11;
	EXTI_Init(&EXTI_InitStructure);

	/* Connect EXTI13 Line to PB13 pin */
	SYSCFG_EXTILineConfig(EXTI_PortSourceGPIOB, EXTI_PinSource13);

	/* Configure EXTI13 line */
	EXTI_InitStructure.EXTI_Line = EXTI_Line13;
	EXTI_Init(&EXTI_InitStructure);

	/* Connect EXTI15 Line to PB15 pin */
	SYSCFG_EXTILineConfig(EXTI_PortSourceGPIOB, EXTI_PinSource15);

	/* Configure EXTI15 line */
	EXTI_InitStructure.EXTI_Line = EXTI_Line15;
	EXTI_Init(&EXTI_InitStructure);

	/* Connect EXTI10 Line to PC10 pin */
	SYSCFG_EXTILineConfig(EXTI_PortSourceGPIOC, EXTI_PinSource10);

	/* Configure EXTI10 line */
	EXTI_InitStructure.EXTI_Line = EXTI_Line10;
	EXTI_Init(&EXTI_InitStructure);


	/* Connect EXTI8 Line to PB8 pin - PMB Control pin **********************/
	SYSCFG_EXTILineConfig(EXTI_PortSourceGPIOB, EXTI_PinSource8);

	/* Configure EXTI8 line */
	EXTI_InitStructure.EXTI_Line = EXTI_Line8;
	EXTI_Init(&EXTI_InitStructure);

#ifdef TACHO_EXTERNAL_INT
	/* Connect EXTI11 Line to PC11 pin - PWR_FAN_TACHO_PIN ******************/
	SYSCFG_EXTILineConfig(EXTI_PortSourceGPIOD, EXTI_PinSource2);

	/* Configure EXTI11 line */
	EXTI_InitStructure.EXTI_Line = EXTI_Line2;
	EXTI_Init(&EXTI_InitStructure);
#endif

	/* Enable and set EXTI3 Interrupt */
	NVIC_InitStructure.NVIC_IRQChannel = EXTI2_3_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPriority = 0x04;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);

	/* Enable and set EXTI11 Interrupt */
	NVIC_InitStructure.NVIC_IRQChannel = EXTI4_15_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPriority = 0x04;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);


}

/******************************************************************************
  * @function   TIM_Modulation_Config
  * @brief      Setup timers to generate frequencies for spread spectrum
  * 			for the regulator switching frequencies.
  * @param      None
  * @retval     None
  *****************************************************************************/
void TIM_Modulation_Config(void)
{
	TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;
	TIM_OCInitTypeDef  TIM_OCInitStructure;
	GPIO_InitTypeDef GPIO_InitStructure;
	NVIC_InitTypeDef NVIC_InitStructure;

	uint16_t PrescalerValue = 0;

	 //  TIM3 and TIM1 clock enable
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3, ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_TIM1, ENABLE);

	  // GPIOA and GPIOC clock enable
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA | RCC_AHBPeriph_GPIOC, ENABLE);

	 //  GPIOC Configuration: TIM3 CH1 (PC6), TIM3 CH2 (PC7), TIM3 CH3 (PC8), TIM3 CH4 (PC9)
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_6 | GPIO_Pin_7 | GPIO_Pin_8 | GPIO_Pin_9;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP ;
	GPIO_Init(GPIOC, &GPIO_InitStructure);

	  // GPIOB Configuration: TIM1 CH4 (PA11)
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_11;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP ;
	GPIO_Init(GPIOA, &GPIO_InitStructure);

	  // Connect TIM Channels to AF1
	GPIO_PinAFConfig(GPIOC, GPIO_PinSource6, GPIO_AF_1);
	GPIO_PinAFConfig(GPIOC, GPIO_PinSource7, GPIO_AF_1);
	GPIO_PinAFConfig(GPIOC, GPIO_PinSource8, GPIO_AF_1);
	GPIO_PinAFConfig(GPIOC, GPIO_PinSource9, GPIO_AF_1);

	GPIO_PinAFConfig(GPIOA, GPIO_PinSource11, GPIO_AF_2);

		/* Enable the TIM3 global Interrupt */
	NVIC_InitStructure.NVIC_IRQChannel = TIM3_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPriority = 0x06;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);

		/* Enable the TIM1 global Interrupt */
	NVIC_InitStructure.NVIC_IRQChannel = TIM1_CC_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPriority = 0x06;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);

	  /* ---------------------------------------------------------------------------
		TIM3 Configuration: Output Compare Toggle Mode:

		TIM3 input clock (TIM3CLK) is set to APB1 clock (PCLK1).
		  => TIM3CLK = PCLK1 = 48 MHz

		 CC1 update rate = TIM3 counter clock / CCR1_Val = 20869.6 Hz
		   ==> So the TIM3 Channel 1 generates a periodic signal with a
			   frequency equal to 10434.8 Hz.

		 CC2 update rate = TIM3 counter clock / CCR2_Val = 20000 Hz
		   ==> So the TIM3 Channel 2 generates a periodic signal with a
			   frequency equal to 10000 Hz.

		 CC3 update rate = TIM3 counter clock / CCR3_Val = 19354.8 Hz
		   ==> So the TIM3 Channel 3 generates a periodic signal with a
			   frequency equal to 9677.4 Hz.

		 CC4 update rate = TIM3 counter clock / CCR4_Val = 18750 Hz
		   ==> So the TIM3 Channel 4 generates a periodic signal with a
			   frequency equal to 9375 Hz.
	  ---------------------------------------------------------------------------
	  TIM1 Configuration: Output Compare Toggle Mode:
	  CC4 update rate = TIM3 counter clock / CCR4_Val = 21333.3 Hz
		   ==> So the TIM3 Channel 4 generates a periodic signal with a
			   frequency equal to 10666.6 Hz.
	  */

	  /* Timer 3 ***********************************************************/
	  /* Time base configuration */
	TIM_TimeBaseStructure.TIM_Period = 65535;
	TIM_TimeBaseStructure.TIM_Prescaler = PrescalerValue;
	TIM_TimeBaseStructure.TIM_ClockDivision = 0;
	TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;

	TIM_TimeBaseInit(TIM3, &TIM_TimeBaseStructure);

	  /* Output Compare Toggle Mode configuration: Channel1 */
	TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_Toggle;
	TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
	TIM_OCInitStructure.TIM_Pulse = CCR1_Val;
	TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_Low;
	TIM_OC1Init(TIM3, &TIM_OCInitStructure);

	TIM_OC1PreloadConfig(TIM3, TIM_OCPreload_Disable);

	  /* Output Compare Toggle Mode configuration: Channel2 */
	TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
	TIM_OCInitStructure.TIM_Pulse = CCR2_Val;

	TIM_OC2Init(TIM3, &TIM_OCInitStructure);

	TIM_OC2PreloadConfig(TIM3, TIM_OCPreload_Disable);

	  /* Output Compare Toggle Mode configuration: Channel3 */
	TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
	TIM_OCInitStructure.TIM_Pulse = CCR3_Val;

	TIM_OC3Init(TIM3, &TIM_OCInitStructure);

	TIM_OC3PreloadConfig(TIM3, TIM_OCPreload_Disable);

	  /* Output Compare Toggle Mode configuration: Channel4 */
	TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
	TIM_OCInitStructure.TIM_Pulse = CCR4_Val;

	TIM_OC4Init(TIM3, &TIM_OCInitStructure);

	TIM_OC4PreloadConfig(TIM3, TIM_OCPreload_Disable);

	  /* TIM enable counter */
	TIM_Cmd(TIM3, ENABLE);

	  /* TIM IT enable */
	TIM_ITConfig(TIM3, TIM_IT_CC1 | TIM_IT_CC2 | TIM_IT_CC3 | TIM_IT_CC4, ENABLE);


	  /* Timer 1 ***************************************************************/
	  /* Time base configuration */
	TIM_TimeBaseStructure.TIM_Period = 65535;
	TIM_TimeBaseStructure.TIM_Prescaler = PrescalerValue;
	TIM_TimeBaseStructure.TIM_ClockDivision = 0;
	TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;

	TIM_TimeBaseInit(TIM1, &TIM_TimeBaseStructure);

	  /* Output Compare Toggle Mode configuration: Channel4 */
	TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_Toggle;
	TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
	TIM_OCInitStructure.TIM_Pulse = CCR5_Val;
	TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_Low;
	TIM_OC4Init(TIM1, &TIM_OCInitStructure);

	TIM_OC4PreloadConfig(TIM1, TIM_OCPreload_Disable);

	  /* TIM enable counter */
	TIM_Cmd(TIM1, ENABLE);

	  /* TIM IT enable */
	TIM_ITConfig(TIM1, TIM_IT_CC4, ENABLE);

	  /* TIM1 Main Output Enable */
	TIM_CtrlPWMOutputs(TIM1, ENABLE);
}


/******************************************************************************
  * @function   Delay
  * @brief      Inserts a delay time.
  * @param      nTime: specifies the delay time length, in miliseconds.
  * @retval     None
  *****************************************************************************/
void Delay(volatile uint32_t nTime)
{
  TimingDelay = nTime;

  while(TimingDelay != 0u);
}

/******************************************************************************
  * @function   TimingDelay_Decrement
  * @brief      Decrements the TimingDelay variable.
  * @param      None
  * @retval     None
  *****************************************************************************/
void TimingDelay_Decrement(void)
{
  if (TimingDelay != 0x00)
  {
    TimingDelay--;
  }
}

/******************************************************************************
  * @function   Count_Average
  * @brief      Counts average value.
  * @param      value: input array - average is calculated with its data
  * @retval     average value
  *****************************************************************************/
uint16_t Count_Average(const uint16_t value[])
{
	uint16_t average = 0u;
	uint32_t sum = 0u;
	uint8_t i;

	for (i = 0; i < AVERAGE_BUFFER_SIZE; i++)
	{
		sum += value[i];
	}

	average = sum / AVERAGE_BUFFER_SIZE;

	return average;
}

/******************************************************************************
  * @function   LED_Reset_Blinky
  * @brief      Toggles the leds after the reset.
  * @param      None
  * @retval     None
  *****************************************************************************/
void LED_Reset_Blinky(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;
	uint8_t i;

	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOC, ENABLE);

	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_8 | GPIO_Pin_9;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP ;
	GPIO_Init(GPIOC, &GPIO_InitStructure);

	for (i = 0; i < 2; i++)
	{
		GPIO_SetBits(GPIOC, GPIO_Pin_8);
		Delay(200);
		GPIO_ResetBits(GPIOC, GPIO_Pin_8);
		Delay(200);
		GPIO_SetBits(GPIOC, GPIO_Pin_9);
		Delay(200);
		GPIO_ResetBits(GPIOC, GPIO_Pin_9);
		Delay(200);
	}

}

/******************************************************************************
  * @function   Wait_For_I2C_Flag
  * @brief      Checks I2C status + handels timeout.
  * @param      flag: I2C flag name to be checked
  * @retval     None.
  *****************************************************************************/
void Wait_For_I2C_Flag(uint32_t flag)
{
	uint32_t timeout = 2000000u;

	/* I2Cstatus: 0 - no error; 1 - timeout */

	if(flag == I2C_FLAG_BUSY)
	{
		while(I2C_GetFlagStatus(I2C1, flag) != RESET)
		{
			if(timeout-- == 0u)
			{
				I2Cstatus = Timeout;
				return;
			}
		}
	}
	else
	{
		while(I2C_GetFlagStatus(I2C1, flag) == RESET)
		{
			if(timeout-- == 0u)
			{
				I2Cstatus = Timeout;
				return;
			}
		}
	}
}


