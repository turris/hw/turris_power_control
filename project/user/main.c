/**
 ******************************************************************************
 * @file    main.c
 * @author  J. Kovanda & T. Rykl & Z. Kos
 * @date    18-April-2014
 * @brief   Main program body
 ******************************************************************************
 ******************************************************************************
 **/

/* Includes ------------------------------------------------------------------*/
#include "hw_config.h"
#include "power_monitor.h"
#include "serial.h"

/*******************************************************************************
 * @brief  Main program.
 * @param  None
 * @retval None
 *****************************************************************************/
int main (void)
{
    /* Init system clock and PLLs */
    SystemInit();

    /* Setup SysTick Timer for 1 msec interrupts */
    Systick_Config();

    /* Configuration of enable and power good signals for the regulator */
    EN_And_PG_Signals_Config();

    /* Setup USART1 */
    setup_usart1();
    write_line("\r\nStarting up\r\n");

    /* Reset all power signals */
    reset_all_power_signals();

    /* Power up sequence of the regulators after the reset */
    Start_Regulators();

    /* DMA configuration for data transfer from ADC to memory */
    DMA_Config();

    /* AD converter configuration */
    ADC_Config();

    /* Timer configuration for trigger DMA/ADC */
    TIM15_Config();

    /* External interrupts configuration */
    EXTI_Config();

    while(1)
    {
	/* Monitor the power good signals (via interrupts) and manage next reactions */
	Power_Monitor();
    }
}
