/**
  ******************************************************************************
  * @file    stm32f10x_it.c
  * @author  MCD Application Team
  * @version V3.3.0
  * @date    04/16/2010
  * @brief   Main Interrupt Service Routines.
  *          This file provides template for all exceptions handler and peripherals
  *          interrupt service routine.
  ******************************************************************************
  * @copy
  *
  * THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
  * WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE
  * TIME. AS A RESULT, STMICROELECTRONICS SHALL NOT BE HELD LIABLE FOR ANY
  * DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING
  * FROM THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
  * CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
  *
  * <h2><center>&copy; COPYRIGHT 2010 STMicroelectronics</center></h2>
  */

/* Includes ------------------------------------------------------------------*/
#include "stm32f0xx_it.h"
#include "hw_config.h"
#include "project_config.h"


/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
volatile uint8_t dataReceiveFlag;
extern volatile uint8_t PG_DownFlag_PB3;
extern volatile uint8_t PG_DownFlag_PB11;
extern volatile uint8_t PG_DownFlag_PB13;
extern volatile uint8_t PG_DownFlag_PB15;
extern volatile uint8_t PG_DownFlag_PC10;
extern volatile uint8_t PMB_ControlResetFlag;
volatile uint16_t pulsCounter = 0u;
volatile uint16_t fanFreq;
extern volatile uint16_t CCR1_Val;
extern volatile uint16_t CCR2_Val;
extern volatile uint16_t CCR3_Val;
extern volatile uint16_t CCR4_Val;
extern volatile uint16_t CCR5_Val;
volatile uint8_t DMAEndOfTransfer;

/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/

/******************************************************************************/
/*            Cortex-M3 Processor Exceptions Handlers                         */
/******************************************************************************/

/**
  * @brief  This function handles NMI exception.
  * @param  None
  * @retval None
  */
void NMI_Handler(void)
{
}

/**
  * @brief  This function handles Hard Fault exception.
  * @param  None
  * @retval None
  */
void HardFault_Handler(void)
{
  /* Go to infinite loop when Hard Fault exception occurs */
  while (1)
  {
  }
}

/**
  * @brief  This function handles Memory Manage exception.
  * @param  None
  * @retval None
  */
void MemManage_Handler(void)
{
  /* Go to infinite loop when Memory Manage exception occurs */
  while (1)
  {
  }
}

/**
  * @brief  This function handles Bus Fault exception.
  * @param  None
  * @retval None
  */
void BusFault_Handler(void)
{
  /* Go to infinite loop when Bus Fault exception occurs */
  while (1)
  {
  }
}

/**
  * @brief  This function handles Usage Fault exception.
  * @param  None
  * @retval None
  */
void UsageFault_Handler(void)
{
  /* Go to infinite loop when Usage Fault exception occurs */
  while (1)
  {
  }
}

/**
  * @brief  This function handles SVCall exception.
  * @param  None
  * @retval None
  */
void SVC_Handler(void)
{
}

/**
  * @brief  This function handles Debug Monitor exception.
  * @param  None
  * @retval None
  */
void DebugMon_Handler(void)
{
}

/**
  * @brief  This function handles PendSV_Handler exception.
  * @param  None
  * @retval None
  */
void PendSV_Handler(void)
{
}

/**
  * @brief  This function handles SysTick Handler.
  * @param  None
  * @retval None
  */
void SysTick_Handler(void)
{
	TimingDelay_Decrement();
}

/******************************************************************************/
/*            STM32F10x Peripherals Interrupt Handlers                        */
/******************************************************************************/


/**
  * @brief  This function handles DMA1 Channel 1 interrupt request.
  * @param  None
  * @retval None
  */
void DMA1_Channel1_IRQHandler(void)
{
  /* Test on DMA1 Channel1 Transfer Complete interrupt */
  if(DMA_GetITStatus(DMA1_IT_TC1))
  {
    /* DMA1 finished the transfer */
	DMAEndOfTransfer = 1u;

	//DMA_Cmd(DMA1_Channel1, DISABLE);
	//DMA_ITConfig(DMA1_Channel1, DMA_IT_TC, DISABLE);

	/* Clear DMA1 Channel1 Half Transfer, Transfer Complete and Global interrupt pending bits */
	DMA_ClearITPendingBit(DMA1_IT_GL1);
  }

  if(DMA_GetITStatus(DMA1_IT_TE1))
   {

	  DMA_ClearITPendingBit(DMA1_IT_TE1);
   }
}

/**
  * @brief  This function handles I2C1 interrupt request.
  * @param  None
  * @retval None
  */
void I2C1_IRQHandler(void)
{
	static uint8_t I2C_AddressMatched = 0u;
	uint8_t slaveData;

  /* Test on I2C1 Address match interrupt */
  if(I2C_GetITStatus(PMB_I2C_PERIPH_NAME, I2C_IT_ADDR) == SET)
  {
	  I2C_AddressMatched = 1u;

	  /* Clear IT pending bit */
	  I2C_ClearITPendingBit(PMB_I2C_PERIPH_NAME, I2C_IT_ADDR);
  }

  /* Test on I2C1 receive data present interrupt with previous address matched interrupt */
  if ( (I2C_GetITStatus(PMB_I2C_PERIPH_NAME, I2C_IT_RXNE) == SET) && (I2C_AddressMatched == 1u))
  {
	  slaveData = I2C_ReceiveData(PMB_I2C_PERIPH_NAME);
	  dataReceiveFlag = 1u;
	  I2C_AddressMatched = 0u;
  }
}

/**
  * @brief  This function handles External line 2 to 3 interrupt request.
  * @param  None
  * @retval None
  */

void EXTI2_3_IRQHandler(void)
{
  if(EXTI_GetITStatus(EXTI_Line3) != RESET)
  {
	  PG_DownFlag_PB3 = 1u;

    /* Clear the EXTI line 3 pending bit */
    EXTI_ClearITPendingBit(EXTI_Line3);
  }
#ifdef TACHO_EXTERNAL_INT
  if(EXTI_GetITStatus(EXTI_Line2) != RESET)
  {
	  pulsCounter++;

    /* Clear the EXTI line 11 pending bit */
    EXTI_ClearITPendingBit(EXTI_Line2);
  }
#endif
}


/**
  * @brief  This function handles External lines 4 to 15 interrupt request.
  * @param  None
  * @retval None
  */
void EXTI4_15_IRQHandler(void)
{

  if(EXTI_GetITStatus(EXTI_Line10) != RESET)
  {
	  PG_DownFlag_PC10 = 1u;

    /* Clear the EXTI line 10 pending bit */
    EXTI_ClearITPendingBit(EXTI_Line10);
  }

  if(EXTI_GetITStatus(EXTI_Line11) != RESET)
    {
  	  PG_DownFlag_PB11 = 1u;

      /* Clear the EXTI line 11 pending bit */
      EXTI_ClearITPendingBit(EXTI_Line11);
    }


  if(EXTI_GetITStatus(EXTI_Line13) != RESET)
  {
	  PG_DownFlag_PB13 = 1u;

    /* Clear the EXTI line 13 pending bit */
    EXTI_ClearITPendingBit(EXTI_Line13);
  }

  if(EXTI_GetITStatus(EXTI_Line15) != RESET)
  {
	  PG_DownFlag_PB15 = 1u;

    /* Clear the EXTI line 15 pending bit */
    EXTI_ClearITPendingBit(EXTI_Line15);
  }

  if(EXTI_GetITStatus(EXTI_Line8) != RESET)
  {
	  PMB_ControlResetFlag = 1u;

    /* Clear the EXTI line 8 pending bit */
    EXTI_ClearITPendingBit(EXTI_Line8);
  }
}

/**
  * @brief  This function handles TIM3 global interrupt request.
  * @param  None
  * @retval None
  */
void TIM3_IRQHandler(void)
{
	uint16_t capture = 0;

  /* TIM3_CH1 toggling with frequency = 10434.8 Hz */
  if (TIM_GetITStatus(TIM3, TIM_IT_CC1) != RESET)
  {
    TIM_ClearITPendingBit(TIM3, TIM_IT_CC1 );
    capture = TIM_GetCapture1(TIM3);
    TIM_SetCompare1(TIM3, capture + CCR1_Val );
  }

  /* TIM3_CH2 toggling with frequency = 10000 Hz */
  if (TIM_GetITStatus(TIM3, TIM_IT_CC2) != RESET)
  {
    TIM_ClearITPendingBit(TIM3, TIM_IT_CC2);
    capture = TIM_GetCapture2(TIM3);
    TIM_SetCompare2(TIM3, capture + CCR2_Val);
  }

  /* TIM3_CH3 toggling with frequency = 9677.4 Hz */
  if (TIM_GetITStatus(TIM3, TIM_IT_CC3) != RESET)
  {
    TIM_ClearITPendingBit(TIM3, TIM_IT_CC3);
    capture = TIM_GetCapture3(TIM3);
    TIM_SetCompare3(TIM3, capture + CCR3_Val);
  }

  /* TIM3_CH4 toggling with frequency = 9375 Hz */
  if (TIM_GetITStatus(TIM3, TIM_IT_CC4) != RESET)
  {
    TIM_ClearITPendingBit(TIM3, TIM_IT_CC4);
    capture = TIM_GetCapture4(TIM3);
    TIM_SetCompare4(TIM3, capture + CCR4_Val);
  }
}

/**
  * @brief  This function handles TIM1 global interrupt request.
  * @param  None
  * @retval None
  */
void TIM1_CC_IRQHandler(void)
{
	uint16_t capture2 = 0;

   /* TIM1_CH4 toggling with frequency = 10666.6 Hz */
	if (TIM_GetITStatus(TIM1, TIM_IT_CC4) != RESET)
	{
	  TIM_ClearITPendingBit(TIM1, TIM_IT_CC4);
	  capture2 = TIM_GetCapture4(TIM1);
	  TIM_SetCompare4(TIM1, capture2 + CCR5_Val);
	}
}

/**
  * @brief  This function handles TIM14 global interrupt request.
  * @param  None
  * @retval None
  */
void TIM14_IRQHandler(void)
{
	if (TIM_GetITStatus(TIM14, TIM_IT_Update) != RESET)
	{
	  TIM_ClearITPendingBit(TIM14, TIM_IT_Update);
	  fanFreq = pulsCounter / TIMER_PERIOD;

	}
}

/**
  * @brief  This function handles TIM14 global interrupt request.
  * @param  None
  * @retval None
  */
void TIM16_IRQHandler(void)
{
	if (TIM_GetITStatus(TIM16, TIM_IT_Update) != RESET)
	{
		TIM_ClearITPendingBit(TIM16, TIM_IT_Update);
			//GPIO_SetBits(GPIOD, PWR_FAN_PWM_PIN);
		SET_FAN_PWM_PIN;
	}

	if(TIM_GetITStatus(TIM16, TIM_IT_CC1) != RESET)
	{
		TIM_ClearITPendingBit(TIM16, TIM_IT_CC1);
			//GPIO_ResetBits(GPIOD, PWR_FAN_PWM_PIN);
		RESET_FAN_PWM_PIN;
	}
}

/**
  * @}
  */

/**
  * @}
  */

/******************* (C) COPYRIGHT 2010 STMicroelectronics *****END OF FILE****/
