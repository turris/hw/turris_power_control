/**
  ******************************************************************************
  * @file    power_monitor.h
  * @author  J. Kovanda & T. Rykl & Z. Kos
  * @date    10-July-2014
  * @brief   Header file power monitor file
  ******************************************************************************
  ******************************************************************************
**/

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __POWER_MONITOR_H
#define __POWER_MONITOR_H

#include "stm32f0xx.h"

/* Private typedef -----------------------------------------------------------*/

/* Function prototypes -------------------------------------------------------*/

/******************************************************************************
  * @function   Start_Regulators
  * @brief      Power up sequence - checks PMB_CNTRL pin and starts regulators.
  * 			It checks their PG pins.
  * @param      None.
  * @retval     None.
  *****************************************************************************/
void Start_Regulators(void);

/******************************************************************************
  * @function   Power_Monitor
  * @brief      Monitors the power good signals (via interrupts) and manages next
  * 			reactions.
  * 			 - Checks PMB_CNTRL pin -> SW reset.
  * 			 - Checks PG signals -> disable regulators
  * @param      None.
  * @retval     None.
  *****************************************************************************/
void Power_Monitor(void);

void reset_all_power_signals(void);



#endif /* __POWER_MONITOR_H */
