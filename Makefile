PROJ_NAME=turris_power_control

################################################################################
#                   SETUP TOOLS                                                #
################################################################################

CC      = arm-none-eabi-gcc
OBJCOPY = arm-none-eabi-objcopy
AS      = arm-none-eabi-as
SIZE	= arm-none-eabi-size

##### Preprocessor options

# #defines needed when working with the STM peripherals library
DEFS 	= -DSTM32F030R8T6
DEFS   += -DSTM32F030X8
DEFS   += -DUSE_STDPERIPH_DRIVER
#DEFS   += -DUSE_FULL_ASSERT
DEFS   += -D__ASSEMBLY_

##### Assembler options

AFLAGS  = -mcpu=cortex-m0 
AFLAGS += -mthumb
AFLAGS += -mlittle-endian

##### Compiler options

CFLAGS  = -ggdb
CFLAGS += -O0
CFLAGS += -Wall -Wextra -Warray-bounds #-pedantic
CFLAGS += $(AFLAGS)
CFLAGS += -nostdlib
CFLAGS += -ffunction-sections

##### Linker options

# tell ld which linker file to use
# (this file is in the current directory)
#  -Wl,...:     tell GCC to pass this to linker.
#    -Map:      create map file
#    --cref:    add cross reference to  map file
LFLAGS  = -T$(LINKER_DIR)/STM32F0308_FLASH.ld
LFLAGS +="-Wl,-Map=$(PROJ_NAME).map",--cref
LFLAGS += -nostartfiles
LFLAGS += -gc-sections

# directories to be searched for header files
INCLUDE = $(addprefix -I,$(INC_DIRS))

################################################################################
#                   SOURCE FILES DIRECTORIES                                   #
################################################################################
LINKER_DIR = project/linker

STM_ROOT        = project/stm32f0xx_stdperiph_driver

USER_SRC_DIR	= project/user
STM_SRC_DIR     = $(STM_ROOT)/src
STM_CMSIS_DIR 	= project/cmsis_boot
STM_STARTUP_DIR = $(STM_CMSIS_DIR)/startup

vpath %.c $(USER_SRC_DIR)
vpath %.c $(STM_SRC_DIR)
vpath %.c $(STM_CMSIS_DIR)
vpath %.s $(STM_STARTUP_DIR)

################################################################################
#                   HEADER FILES DIRECTORIES                                   #
################################################################################

# The header files we use are located here
INC_DIRS += $(STM_ROOT)/inc
INC_DIRS += $(STM_CMSIS_DIR)
INC_DIRS += $(USER_SRC_DIR)
INC_DIRS += project/cmsis_core

################################################################################
#                   SOURCE FILES TO COMPILE                                    #
################################################################################

SRCS  += main.c
SRCS  += system_stm32f0xx.c
SRCS  += hw_config.c
SRCS  += power_monitor.c
SRCS  += stm32f0xx_it.c
SRCS  += serial.c 

################# STM LIB ##########################
SRCS  += stm32f0xx_adc.c
SRCS  += stm32f0xx_rcc.c
SRCS  += stm32f0xx_gpio.c
SRCS  += stm32f0xx_tim.c
SRCS  += stm32f0xx_dma.c
SRCS  += stm32f0xx_dbgmcu.c
SRCS  += stm32f0xx_exti.c
SRCS  += stm32f0xx_i2c.c
SRCS  += stm32f0xx_syscfg.c
SRCS  += stm32f0xx_misc.c
SRCS  += stm32f0xx_usart.c

# startup file, calls main
ASRC  = startup_stm32f030.s

OBJS  = $(SRCS:.c=.o)
OBJS += $(ASRC:.s=.o)

################################################################################
#                         SIZE OF OUTPUT                                       #
################################################################################
ELFSIZE = $(SIZE) -d $(PROJ_NAME).elf

buildsize: $(PROJ_NAME).elf
	@echo Program Size: 
	$(ELFSIZE)

################################################################################
#                         SETUP TARGETS                                        #
################################################################################

.PHONY: all

all: $(PROJ_NAME).elf buildsize
						
$(PROJ_NAME).elf: $(OBJS)
	@echo "[Linking    ]  $@"	
	@$(CC) $(CFLAGS) $(LFLAGS) $(INCLUDE) $^ -o $@
	@$(OBJCOPY) -O ihex $(PROJ_NAME).elf   $(PROJ_NAME).hex
	@$(OBJCOPY) -O binary $(PROJ_NAME).elf $(PROJ_NAME).bin

%.o : %.c
	@echo "[Compiling  ]  $^"
	$(CC) -c $(DEFS) $(CFLAGS) $(INCLUDE) $< -o $@

%.o : %.s 
	@echo "[Assembling ]" $^
	@$(AS) $(AFLAGS) $< -o $@

clean:
	rm -r -f *.o $(PROJ_NAME).elf $(PROJ_NAME).hex $(PROJ_NAME).bin $(PROJ_NAME).map

#********************************
# generating of the dependencies
dep:	
	$(CC) $(DEFS) $(CFLAGS) $(INCLUDE) -MM $(USER_SRC_DIR)/*.c > dep.list

## insert generated dependencies
-include dep.list 
#*******************************

